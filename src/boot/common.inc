
%ifndef _COMMON_INC_INCLUDED
%define _COMMON_INC_INCLUDED

; where the kernel is to be loaded to in protected mode
%define IMAGE_PMODE_BASE 0x120000

; where the kernel is to be loaded to in real mode
%define IMAGE_RMODE_BASE 0x6000

; kernel name (Must be 11 bytes)
ImageName     db "STAGE3  SYS"

; size of kernel image in bytes
ImageSize     db 0

%endif
