PML4_Start_Address equ 0x1000

switch_to_long_mode:
	call set_up_page_tables

	call enable_PAE

	
	call switch_to_compatibility_mode
	


	lgdt [GDT64.Pointer]
	jmp GDT64.Code:main64

	ret
	
set_up_page_tables:
	; we'll identity map the first 2MiB of memory

	; first, we zero out space for the 4 page tables
	; we need 4096 bytes per table
	mov EDI, PML4_Start_Address
	mov EAX, 0
	mov ECX, 4096
	rep stosd

	; CR3 points to the PML4
	mov EDI, PML4_Start_Address
	mov CR3, EDI
	

	; the first entry of the PML4 points to the PDPT
	mov dword [EDI], 0x2003

	; the first entry of the PDPT points to the PD
	add EDI, 0x1000
	mov dword [EDI], 0x3003

	; the first entry of the PD points to the PT
	add EDI, 0x1000
	mov dword [EDI], 0x4003

	; identity map the PT entries to physical RAM
	add EDI, 0x1000
	mov EBX, 0x00000003
	mov ECX, 512 ; PT has 512 entries
	setPT_loop:
		mov [EDI], EBX
		add EDI, 8
		add EBX, 0x1000
		loop setPT_loop

	ret

enable_PAE:
	mov EAX, CR4
	or EAX, 1 << 5 ; PAE bit
	mov CR4, EAX
	ret

switch_to_compatibility_mode:
	mov ECX, 0xC0000080
	rdmsr
	or EAX, 1 << 8 ; LM bit
	wrmsr


	mov EAX, CR0
	OR EAX, 1 << 31 ; Paging (PG) bit
	OR EAX, 1 << 0  ; Protected Mode (PM) bit
	mov CR0, EAX
	
	
	

	ret
	
haha_msg db "HAHA", 13, 10, 0