P_bit equ 47
L_bit equ 53
Res12_bit equ 44
Res11_bit equ 43
RW_bit equ 41 ; readable bit


GDT64:
	.Null: equ $ - GDT64
		dq 0

	.Code: equ $ - GDT64
		dq (1 << P_bit) | (1 << L_bit) | (1 << Res12_bit) | (1 << Res11_bit) 

	.Data: equ $ - GDT64
		dq (1 << P_bit) | (1 << Res12_bit) | (1 << RW_bit)

	.Pointer:
		dw $ - GDT64 - 1
		dq GDT64
		
	