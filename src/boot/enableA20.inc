try_enabling_A20:
	; call disable_A20

	call test_A20_enabled
	cmp AL, 1
	je A20_enabled
	mov SI, ENABLING_A20_MSG
	call print
	call enable_A20
	cmp AL, 1
	je A20_enabled
	; failed to enable A20
	mov SI, A20_ENABLING_FAILED_MSG
	call print

	A20_enabled:
		mov SI, A20_ENABLED_MSG
		call print

	ret

enable_A20:
	; test if not already enabled
	call test_A20_enabled
	cmp AL, 1
	je done_enabling


	; try enabling through BIOS interrupt 15h
	call enable_A20_BIOS
	; test if successful
	call test_A20_enabled
	cmp AL, 1
	je done_enabling

	; trye enabling using the keyboard controller
	call enable_A20_keyboard_controller
	; test if successful
	call test_A20_enabled
	cmp AL, 1
	je done_enabling

	; try fast enabling of A20
	call enable_A20_fast
	; test if successful
	call test_A20_enabled
	cmp AL, 1
	je done_enabling

	enabling_failed:
		mov AL, 0
		ret

	done_enabling:
		mov AL, 1
		ret


enable_A20_BIOS:
	; test for A20 gate interrupt support
	mov AX, 0x2403
	int 0x15
	jc enable_A20_BIOS_failed
	cmp AH, 0
	jne enable_A20_BIOS_failed

	; get line status
	mov AX, 0x2402
	int 0x15
	jc enable_A20_BIOS_failed
	cmp AH, 0
	jne enable_A20_BIOS_failed

	; check if not already enabled
	cmp AL, 1
	je enable_A20_BIOS_successful

	; if not already enable, try enabling
	mov AX, 0x2401
	int 0x15
	jc enable_A20_BIOS_failed
	cmp AH, 0
	jne enable_A20_BIOS_failed

	enable_A20_BIOS_successful:
		mov AL, 1
		ret

	enable_A20_BIOS_failed:
		mov AL, 0
		ret

; Enables A20 through the keyboard controller.
; Copy-pasted from https://wiki.osdev.org/A20
enable_A20_keyboard_controller: 
        call    a20wait
        mov     al,0xAD
        out     0x64,al
 
        call    a20wait
        mov     al,0xD0
        out     0x64,al
 
        call    a20wait2
        in      al,0x60
        push    eax
 
        call    a20wait
        mov     al,0xD1
        out     0x64,al
 
        call    a20wait
        pop     eax
        or      al,2
        out     0x60,al
 
        call    a20wait
        mov     al,0xAE
        out     0x64,al
 
        call    a20wait
        ret
 
a20wait:
        in      al,0x64
        test    al,2
        jnz     a20wait
        ret
 
 
a20wait2:
        in      al,0x64
        test    al,1
        jz      a20wait2
        ret
	

; Fast enabling of A20.
; Copy-pasted from https://wiki.osdev.org/A20
enable_A20_fast:
	in al, 0x92
	test al, 2
	jnz fast_enabling_done
	or al, 2
	and al, 0xFE
	out 0x92, al
	fast_enabling_done:
		ret

disable_A20:
	mov AX, 0x2400
	int 0x15

	mov SI, DISABLE_MSG
	call print

	ret

DISABLE_MSG db "[WARNING] A20 disabled on purpose!", 13, 10, 0
ENABLING_A20_MSG db 'Enabling A20 line...', 13, 10, 0
A20_ENABLED_MSG db 'A20 line enabled successfully.', 13, 10, 0
A20_ENABLING_FAILED_MSG db '[FATAL] Unable to enable the A20 line. Aborting bootloader...', 13, 10, 0