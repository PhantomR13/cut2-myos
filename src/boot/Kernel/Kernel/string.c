#include <size_t.h>

#include <string.h>



int strlen(const char* str) {

	int	len = 0;
	while (str[len++]); /* careful! MSVC++ optomization might embed call to strlen()*/
	return len;
}
