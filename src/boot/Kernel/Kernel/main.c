/*
======================================
	main.cpp
		-kernel startup code
======================================
*/

#include "DebugDisplay.h"
#include "main.h"


#include "DebugDisplay.h"

void __cdecl clrscr()
{
	unsigned char* p = (unsigned char*)VID_MEMORY;

	for (int i = 0; i < 160 * 30; i += 2) {

		p[i] = ' ';  /* Need to watch out for MSVC++ optomization memset() call */
		p[i + 1] = 0x0;
	}
}

void __cdecl DisplayMessage(const char *str)
{
	unsigned char* p = (unsigned char*)VID_MEMORY;

	int i = 0;
	char c = 0;

	while ((c = str[i++]) != 0)
	{
		*p++ = c;
		*p++ = 0x18;
	}
}

#define Print(str) {\
unsigned char* p = (unsigned char*)VID_MEMORY;\
\
int i = 0; \
char c = 0; \
\
while ((c = str[i++]) != 0)\
{\
*p++ = c; \
*p++ = 0x18; \
}\
}\


void _cdecl main() 
{
	char *str = "PRINT ME!";
	DisplayMessage(str);

	//DisplayMessage("AAA");




//	for (;;);
}

