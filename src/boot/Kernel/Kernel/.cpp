#include "main.h"

// Note: Some systems may require 0xB0000 Instead of 0xB8000
// We dont care for portability here, so pick either one
#define VID_MEMORY	0xB8000

// these vectors together act as a corner of a bounding rect
// This allows GotoXY() to reposition all the text that follows it



void DebugPutc2(unsigned char c) {
	 int _xPos = 0, _yPos = 0;
	 int _startX = 0, _startY = 0;

	if (c == 0)
		return;

	if (c == '\n' || c == '\r') {	/* start new line */
		_yPos += 2;
		_xPos = _startX;
		return;
	}

	if (_xPos > 79) {			/* start new line */
		_yPos += 2;
		_xPos = _startX;
		return;
	}
	
	/* draw the character */
	unsigned char* p = (unsigned char*)VID_MEMORY + (_xPos++) * 2 + _yPos * 80;
	*p++ = c;
	*p = 0x17;
}

void clrscr(void)
{
	unsigned char* p = (unsigned char*)0xb8000;

	for (int i = 0; i < 160 * 30; i += 2) {

		p[i] = ' ';  /* Need to watch out for MSVC++ optomization memset() call */
		p[i + 1] = 0x18;
	}
}
char *str = "Hello, world";

void test(void)
{
	char *ch;
	unsigned short *vidmem = (unsigned short*)0xb8000;
	unsigned i;

	for (ch = str, i = 0; *ch; ch++, i++)
		vidmem[i] = (unsigned char)*ch | 0x0700;

	for (;;);
}

void main(void)
{
	clrscr();
	test();
	unsigned char *p = VID_MEMORY;
	*p++ = 'a';
	*p = 0x17;

	//x += 5;

	for (;;);
//	DebugPutc2(x);

}