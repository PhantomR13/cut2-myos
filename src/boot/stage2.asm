[org 0x500]

[bits 16]


jmp main

; ;;;;;;;;;;;;;;;;;;;;;;
; Function imports
; ;;;;;;;;;;;;;;;;;;;;;;

%include "bios/print.inc"
%include "enableA20.inc"
%include "testA20.inc"		
%include "check_if_cpuid_supported.inc"
%include "check_if_long_mode_usable.inc"
%include "long_mode.inc"
%include "gdt64.inc"

%include "Fat12.inc"			; FAT12 driver. Kinda :)
%include "common.inc"

; ;;;;;;;;;;;;;;;;;;;;;;
; Data
; ;;;;;;;;;;;;;;;;;;;;;;

start_msg db "Stage 2 Bootloader started.", 13, 10, 0

main:
	cli				; clear interrupts
	xor	ax, ax			; null segments
	mov	ds, ax
	mov	es, ax
	mov	ax, 0x0			; stack begins at 0x9000-0xffff
	mov	ss, ax
	mov	sp, 0xFFFF

	call try_enabling_A20
	
	; Load FAT12 Root
	call	LoadRoot		; Load root directory table]
	
	call check_if_cpuid_supported

	call check_if_long_mode_usable
	
	; Load Kernel
	mov	ebx, 0			; BX:BP points to buffer to load to
    mov	bp, IMAGE_RMODE_BASE
	mov	si, ImageName		; our file to load
	call	LoadFile		; load our file
	mov	dword [ImageSize], ecx	; save size of kernel
	cmp	ax, 0			; Test for success
	call switch_to_long_mode
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; LONG MODE ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
[bits 64]

;;;;;;;;;;
;; DATA
;;;;;;;;;;

long_mode_msg db "I'm in long mode, yeehaw!", 10, 0
kernel_escape_msg db "I've escaped from the kernel, woo!!!", 10, 0

%include "macros.inc"

%include "vga.inc"	


	
main64:
	xor rax, rax
	xor rbx, rbx
	xor rcx, rcx
	xor rdx, rdx
	xor rbp, rbp
	xor rsp, rsp
	xor rdi, rdi
	xor rsi, rsi

	mov	ax, 0
	mov	ds, ax
	mov	ss, ax
	mov	es, ax
	mov	esp, 90000h		; stack begins from 90000h
	
	;-------------------------------;
	; Copy kernel to 1MB		;
	;-------------------------------;

CopyImage:
	cli	
	call clrscr
	
	mov EBX, long_mode_msg
	call print64
	

  	 mov	eax, dword [ImageSize]
  	 movzx	ebx, word [bpbBytesPerSector]
  	 mul	ebx
  	 mov	ebx, 4
  	 div	ebx
   	 cld
   	 mov    esi, IMAGE_RMODE_BASE
   	 mov	edi, IMAGE_PMODE_BASE
   	 mov	ecx, eax
   	 rep	movsd                   ; copy image to its protected mode address

	;---------------------------------------;
	;   Execute Kernel			;
	;---------------------------------------;
		
	; jmp IMAGE_PMODE_BASE
	
	xor rbp, rbp
	
	; LOAD C KERNEL
	mov ebx, [IMAGE_PMODE_BASE+60]	; e_lfanew is a 4 byte offset address of the PE header; it is 60th byte. Get it
	add ebx, IMAGE_PMODE_BASE 		; add base
	
	add		ebx, 24
	mov		eax, [ebx]			; _IMAGE_FILE_HEADER is 20 bytes + size of sig (4 bytes)
	add		ebx, 20-4			; address of entry point
	mov		ebp, dword [ebx]		; get entry point offset in code section	
	add		ebx, 12				; image base is offset 8 bytes from entry point
	mov		eax, dword [ebx]		; add image base
	add		ebp, eax
	mov 	eax, ebp
	
	; xor rax, rax NO!
	xor rbx, rbx
	xor rcx, rcx
	xor rdx, rdx
	xor rbp, rbp
	xor rsp, rsp
	mov rsp, 0x90000
	xor rdi, rdi
	xor rsi, rsi
	
	call rax
	
	mov EBX, kernel_escape_msg
	call print64
	hlt
	;---------------------------------------;
	;   Stop execution			;
	;---------------------------------------;

	cli
	hlt
	


 

hang:
	cli
	hlt

