;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;; VGA
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

%define	VIDMEM	0xB8000		
%define COLS 	80
%define ROWS	25
%define ATTRIB  14

; cursor position
curX db 0
curY db 0

; prints character in BL to the screen
printchar64:
	pushaq
	mov edi, VIDMEM
	
	xor eax, eax
	
	; linear vidmem offset = curx * 2 + cury * COLS * 2 (2 bytes per pixel)
	mov al, [curY]
	mov ecx, COLS * 2
	mul ecx
	push rax
	
	xor eax, eax
	mov al, [curX]
	mov cl, 2
	mul cl
	pop rcx
	add eax, ecx
		
	add edi, eax
	
	; new line
	cmp bl, 10
	je .NewLine
	
	; print 
	mov dl, bl
	mov dh, ATTRIB
	mov word [edi], dx
	
	
	; update pos
	inc byte [curX]
	cmp byte [curX], COLS
	jne .Done
	
	.NewLine:
	mov byte [curX], 0
	inc byte [curY]
		

	.Done:
	
	mov bh, [curY]
	mov bl, [curX]
	call moveCursor64
	
	
	popaq
	
	ret
	
; prints string at EBX to screen
print64:
	pushaq
	
	mov edi, ebx
	
	.PrintLoop:
		mov BL, [EDI]
		cmp BL, 0
		je .Done
		call printchar64
		inc EDI
		jmp .PrintLoop
	
	.Done:
		popaq
		ret
		
; move cursor at location (X,Y)=(BL, BH)  ?
moveCursor64:
	pushaq
	
	xor rax, rax
	
	mov ecx, COLS
	mov al, bh
	mul ecx
	
	xor edx, edx
	mov dl, bl
	add eax, edx
	
	mov ebx, eax ; ebx <- y * COLS
	
	; low byte
	mov al, 0x0f
	mov dx, 0x03D4 ; CRT Index Port
	out dx, al
	
	mov al, bl
	mov dx, 0x03D5
	out dx, al

	; high byte
	mov al, 0x0e
	mov dx, 0x03D4 ; CRT Index Port
	out dx, al
	
	mov al, bh
	mov dx, 0x03D5
	out dx, al

	
	
	popaq
	ret
	
clrscr:
	pushaq
	mov edi, VIDMEM
	mov rcx, ROWS * COLS
	mov ah, ATTRIB
	mov al, ' '
	rep stosw
	
	mov byte [curX], 0
	mov byte [curY], 0
	
	popaq
	ret
		

