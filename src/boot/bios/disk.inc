; Reads AL sectors from the drive specified by DL into the buffer at ES:BX.
; Source address: (CYL, HEAD, SECTOR) = (CH, DH, CL) ; SECTOR is 1-based
; AH will be filled with the return code ; AL will be filled with the read sector count.
readSectorsFromDisk:
	; save the number of sectors requested
	push AX
	mov AH, 0x02
	int 0x13

	; fatal error
	jc fatal_disk_read_error

	; check for success
	cmp AH, 0x00
	jne disk_read_error

	; check if all requested sectors were read
	pop BX
	cmp AL, BL
	jne disk_read_error
	ret

	fatal_disk_read_error:
		mov SI, FATAL_DISK_READ_ERROR_MSG
		call print
		jmp hang

	disk_read_error:
		mov SI, DISK_READ_ERROR_MSG
		call print
		jmp hang


FATAL_DISK_READ_ERROR_MSG db 'Fatal error occured while reading from disk.', 13, 10, 0
DISK_READ_ERROR_MSG db 'Disk read error occured.', 13, 10